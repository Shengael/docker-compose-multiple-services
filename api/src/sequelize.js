const Sequelize = require('sequelize');
const IkeaModel = require('./models/ikea');

console.log(process.env.MYSQL_DATABASE);
console.log(process.env.MYSQL_USER);
console.log(process.env.MYSQL_PASSWORD);
console.log(process.env.MYSQL_HOST);
console.log(process.env.MYSQL_PORT);
const sequelize = new Sequelize(process.env.MYSQL_DATABASE, process.env.MYSQL_USER, process.env.MYSQL_PASSWORD, {
    host: process.env.MYSQL_HOST,
    dialect: 'mysql',
    port: process.env.MYSQL_PORT,
    define: {
        timestamps: false
    }
});

const Ikea = IkeaModel(sequelize, Sequelize);

module.exports = {Ikea};
