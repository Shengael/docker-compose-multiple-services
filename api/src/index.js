const {Ikea} = require('./sequelize');
const cors = require('cors');

const express = require('express');
const app = express();

app.use(cors());
app.get('/', function (req, res) {
  Ikea.findAll().then(ikea => res.json(ikea));
});

app.listen(3000, function () {
  console.log('Example app listening on port 3000!')
});
