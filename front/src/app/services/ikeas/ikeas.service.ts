import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {Ikea} from '../../interfaces/Ikea';
import {environment} from '../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class IkeasService {

  constructor(private http: HttpClient) { }

  getIkeas(): Observable<Ikea[]> {
      return this.http.get<Ikea[]>(`${environment.api_url}/`);
  }
}
