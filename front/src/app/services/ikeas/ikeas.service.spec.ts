import { TestBed } from '@angular/core/testing';

import { IkeasService } from './ikeas.service';

describe('IkeasService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: IkeasService = TestBed.get(IkeasService);
    expect(service).toBeTruthy();
  });
});
