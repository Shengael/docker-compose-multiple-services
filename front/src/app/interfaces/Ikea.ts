export interface Ikea {
    id: number;
    name: string;
    image: string;
    url: string;
}
