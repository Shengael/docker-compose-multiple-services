import {Component, OnInit} from '@angular/core';
import {IkeasService} from './services/ikeas/ikeas.service';
import {Ikea} from './interfaces/Ikea';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  title = 'docker';
  ikeas: Ikea[];

  constructor(private ikeasService: IkeasService) {
  }

    ngOnInit(): void {
      this.ikeasService.getIkeas().subscribe(ikeas => {
          this.ikeas = ikeas;
          console.log(this.ikeas);
      });
    }


}
