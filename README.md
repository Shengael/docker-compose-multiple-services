# docker-compose with multiple services


This project is an example of implementation of a docker environment with several services.

The services are :
 - a mysql database
 - an API with nodeJS
 - a website on nginx server

## Getting Started

These instructions will get you a copy of the project up and running on your local machine.

### Prerequisites

What things you need to install the project and how to install them :
 - [git](https://git-scm.com/downloadsgit)
 - [docker](https://docs.docker.com/install/)
 - [docker-compose](https://docs.docker.com/compose/install/)
 
### get the project and start it

```console
foo@bar:~$ git clone https://gitlab.com/Shengael/docker-compose-multiple-services.git
foo@bar:~$ ./run.sh
```

OR
```console
foo@bar:~$ git clone https://gitlab.com/Shengael/docker-compose-multiple-services.git
foo@bar:~$ docker-compose up
```

**the project is now accessible on http://localhost:8080/**

The running services :
 - Mysql database
    - Host : localhost
    - port : 3010
    - user: bastien
    - password: bastien
    - database: ikea
  - API
    - url : http://localhost:3001/
  - website on nginx server
    - url: http://localhost:8080/ 
